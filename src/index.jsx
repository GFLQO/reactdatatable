import React from 'react';
import ReactDOM from 'react-dom/client';
import ReactDataTable from './components/ReactDataTable';
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <ReactDataTable />
  </React.StrictMode>,
);
