import React, { useState } from 'react';
import styled from 'styled-components';

const UL = styled.ul`
  display: flex;
  flex-direction: row;
`;
const LI = styled.li`
  cursor: pointer;
  list-style: none;
  text-decoration: none;
  margin: 0 10px;
  padding: 5px;
  border: 1px solid #343434;
  border-radius: 5px;
`;

export default function Pagination({
  numberPerPage,
  dataCount,
  onPaginationEvent,
}) {
  console.log(dataCount, numberPerPage);
  const [currP, setCurrP] = useState(1);
  let nbrePage = Math.ceil(dataCount / numberPerPage);
  let pages = [];
  for (let i = 0; i < nbrePage; i++) {
    pages.push(i);
  }
  console.log(pages);
  return (
    <div>
      <UL>
        {pages.map((p, i) => {
          return (
            <LI key={i} onClick={() => onPaginationEvent(i)}>
              {p}
            </LI>
          );
        })}
      </UL>
    </div>
  );
}
