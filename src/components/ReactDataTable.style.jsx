import styled from 'styled-components';

export const LiRow = styled.li``;

export const UlRow = styled.ul``;

export const ThRow = styled.th`
  border: 1px solid black;
  padding: 5px;
`;

export const THead = styled.thead`
  border: 2px solid #343434;
  padding: 5px;
  margin: 5px;
`;

export const Table = styled.table`
  border-collapse: collapse;
  border: 2px solid #343434;
`;

export const Main = styled.div`
  width: 100%;
  margin: auto;
`;

export const TdRow = styled.td`
  border: 1px solid black;
`;
