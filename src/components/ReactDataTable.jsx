import React, { useEffect, useState } from 'react';
import Pagination from './Pagination';
import { ThRow, TdRow, Main, Table, THead } from './ReactDataTable.style';

//tester hjeaders avec modulo %

//Faire des données mockés pour tester l'affichage du module

//Faire pagination ==>
//

export default function App() {
  const default_sens = 1;
  var payload = {
    headers: [
      { name: 'S1', sens: default_sens, type: 'string' },
      { name: 'S2', sens: default_sens, type: 'number' },
      { name: 'S3', sens: default_sens, type: 'string' },
      { name: 'S4', sens: default_sens, type: 'boolean' },
    ],
    data: [
      ['B1DATA', 83, 'S3DATA', true],
      ['A1DfreATA', 42, 'K3DATferA', false],
      ['gzzzre', 95, 'ff', true],
      ['gr-ye', 4, 'ff', false],
      ['ggtrre', 41, 'ff', true],
      ['greppp', 20, 'ff', false],
      ['ggezre', 55, 'ff', false],
      ['gre', 45, 'ff', false],
      ['frvbfdèe', 23, 'fz', false],
      ['trre', 40, 'gg', false],
      ['azaaa', 47, 'gtrd', false],
      ['CHH256', 667, 'THOPSQHO', false],
      ['loi', 51, 'dd', false],
      ['dfg', 662, 'af', false],
      ['qg', 7, 'THOPSQHO', false],
      ['eeee', 777, 'THOPSQHO', false],
      ['ggezre', 55, 'ff', false],
      ['gre', 45, 'ff', false],
      ['frvbfdèe', 23, 'fz', false],
      ['miaou', 40, 'gg', false],
      ['vxt', 47, 'gtrd', false],
      ['CHH256', 667, 'THOPSQHO', false],
      ['loi', 51, 'dd', false],
      ['dfg', 662, 'af', false],
      ['qg', 7, 'THOPSQHO', false],
      ['yyyy', 777, 'THOPSQHO', false],
      ['uuuu', 55, 'ff', false],
      ['oooo', 45, 'ff', false],
      ['pppp', 23, 'fz', false],
      ['michou', 40, 'gg', false],
      ['jsdfyvxt', 47, 'gtrd', false],
      ['CHH256', 667, 'THOPSQHO', false],
      ['ffff', 51, 'dd', false],
      ['gggg', 662, 'af', false],
      ['qg', 7, 'THOPSQHO', false],
      ['yuyyy', 777, 'momomo', false],
      ['ggezre', 55, 'ff', false],
      ['gre', 45, 'ff', false],
      ['frvbfdèe', 23, 'fz', false],
      ['trre', 40, 'g', false],
      [' nnnn', 47, 'gtrd', false],
      ['CHH256', 667, 'moiiiitiii', false],
      ['loi', 51, 'dd', false],
      ['dfg', 662, 'af', false],
      ['qg', 7, 'THOPSQHO', false],
      ['vtvt', 777, 'THOPSQHO', false],
      ['ggezre', 55, 'ff', false],
      ['gre', 45, 'ff', false],
      ['bbbb', 23, 'fz', false],
      ['dddd', 40, 'g', false],
      ['ssss', 47, 'gtrd', false],
      ['qqqq', 667, 'THOPSQHO', false],
      ['mmmm', 51, 'dd', false],
      ['rrrr', 662, 'af', false],
      ['qg', 7, 'THOPSQHO', false],
      ['vtvt', 777, 'THOPSQHO', false],
      ['ggezre', 55, 'ff', false],
      ['gre', 45, 'ff', false],
      ['frvbfdèe', 23, 'fz', false],
      ['myCause', 40, 'gg', false],
      ['jsdfyvxt', 47, 'gtrd', false],
      ['CHH256', 667, 'THOPSQHO', false],
      ['loi', 51, 'dd', false],
      ['dfg', 662, 'af', false],
      ['qg', 7, 'THOPSQHO', false],
      ['vtvt', 777, 'THOPSQHO', false],
    ],
  };
  return ReactDataTable(payload);
}
export function ReactDataTable(payload) {
  const [isPagination, setPagination] = useState(false);
  const [startData, setStartData] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [recordsPerPage] = useState(10);
  const [active, setActive] = useState(0);
  const [data, setData] = useState(payload.data);
  const [headers, setHeaders] = useState(payload.headers);
  if (payload.hasOwnProperty('headers') && payload.hasOwnProperty('data')) {
    if (payload.headers.length !== payload.data[0].length) {
      console.log('PAS BON CA');
    }
  }

  useEffect(() => {
    if (data.length > 10) {
      //setPagination(true);
      //pageOK();
      console.log('page :', currentPage);
    }
  }, [active, currentPage]);

  console.log(headers, data);

  // function setPaginationMehtod() {
  //   if (data.length > 10) {
  //     setPagination(true);
  //   }
  // }

  function sortBy(element) {
    const indexToSort = headers.indexOf(element);
    element.sens *= -1;
    const sens = element.sens;
    const type = element.type;
    if (type === 'string') {
      data.sort((a, b) => sens * a[indexToSort].localeCompare(b[indexToSort]));
    } else if (type === 'number') {
      data.sort((a, b) => sens * (a[indexToSort] - b[indexToSort]));
    } else if (type === 'boolean') {
      data.sort((a, b) => sens * a[indexToSort] - b[indexToSort]);
      console.log('false');
    }
    setData(data);
    setHeaders(headers);
    setActive(active + 1);
  }
  return (
    <Main>
      <Table>
        <THead>
          <tr>
            {headers?.map((element, index) => {
              return (
                <ThRow key={index}>
                  {element.name}{' '}
                  <button onClick={() => sortBy(element)}>
                    {element.sens === 1 ? 'V' : '^'}
                  </button>{' '}
                </ThRow>
              );
            })}
          </tr>
        </THead>
        <tbody>
          {data
            ?.slice(
              currentPage * recordsPerPage,
              Math.min(data.length, (currentPage + 1) * recordsPerPage),
            )
            .map((e, indx) => {
              return (
                <tr key={indx}>
                  {e.map((f, i) => {
                    return (
                      <TdRow key={i}>
                        {typeof f === 'boolean' ? f.toString() : f}
                      </TdRow>
                    );
                  })}
                </tr>
              );
            })}
        </tbody>
      </Table>
      {data?.length > recordsPerPage ? (
        <Pagination
          numberPerPage={10}
          dataCount={data.length}
          onPaginationEvent={(e) => setCurrentPage(e)}
        />
      ) : (
        ''
      )}
    </Main>
  );
}

//ReactDataTable(payload);
